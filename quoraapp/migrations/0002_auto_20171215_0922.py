# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-12-15 09:22
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('quoraapp', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AnswerQuestion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ans_question', models.CharField(max_length=10000)),
                ('time', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='AskQuestion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('newquestion_text', models.CharField(max_length=10000)),
                ('time', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('comment', models.CharField(max_length=10000)),
            ],
        ),
        migrations.CreateModel(
            name='Topics',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('topic_name', models.CharField(max_length=1000)),
                ('topic_type', models.CharField(max_length=100)),
            ],
        ),
        migrations.DeleteModel(
            name='AddQuestion',
        ),
        migrations.RemoveField(
            model_name='questions',
            name='Person',
        ),
        migrations.DeleteModel(
            name='Topstories',
        ),
        migrations.RemoveField(
            model_name='upvote',
            name='Person',
        ),
        migrations.AddField(
            model_name='person',
            name='description',
            field=models.CharField(default=0, max_length=1000),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='person',
            name='mail_id',
            field=models.CharField(default=1, max_length=1000),
            preserve_default=False,
        ),
        migrations.DeleteModel(
            name='Questions',
        ),
        migrations.DeleteModel(
            name='Upvote',
        ),
        migrations.AddField(
            model_name='comment',
            name='Person',
            field=models.ManyToManyField(to='quoraapp.Person'),
        ),
        migrations.AddField(
            model_name='askquestion',
            name='Person',
            field=models.ManyToManyField(to='quoraapp.Person'),
        ),
        migrations.AddField(
            model_name='answerquestion',
            name='Person',
            field=models.ManyToManyField(to='quoraapp.Person'),
        ),
    ]
