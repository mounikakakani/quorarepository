# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-12-15 10:30
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('quoraapp', '0008_auto_20171215_1024'),
    ]

    operations = [
        migrations.AddField(
            model_name='question',
            name='Topics',
            field=models.ManyToManyField(to='quoraapp.Topics'),
        ),
    ]
