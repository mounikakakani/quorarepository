# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Person
from .models import Question
from .models import Topics
from .models import Comment
from .models import Answer

# Register your models here.
admin.site.register(Person)
admin.site.register(Question)
admin.site.register(Topics)
admin.site.register(Comment)
admin.site.register(Answer)
