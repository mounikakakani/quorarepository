# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Person(models.Model):
    GENDER_CHOICES = [
                 ('MALE', 'M'),
                 ('FEMALE', 'F'),
    ]
    name = models.CharField(max_length=1000, unique=True)
    mail_id = models.CharField(max_length=1000)
    description = models.CharField(max_length=1000)
    gender = models.CharField(max_length=100, choices=GENDER_CHOICES)

    def __str__(self):
        return self.name




# class Topstories(models.Model):
#     story =models.CharField(max_length=1000)

# class Questions(models.Model):
#     question_text = models.CharField(max_length=1000)
#     Userprofile=models.ManyToManyField(Userprofile)
class Topics(models.Model):
    topic_name=models.CharField(max_length=1000)
    topic_type=models.CharField(max_length=100)
    topic_description = models.CharField(max_length=1000)
    Person = models.ForeignKey(Person)
    def __str__(self):
        return self.topic_name

class Question(models.Model):
    newquestion_text=models.CharField(max_length=10000)
    # Person = models.ForeignKey(Person)
    time = models.DateTimeField()
    Topics = models.ManyToManyField(Topics)
    def __str__(self):
        return self.newquestion_text

class Answer(models.Model):
    answer=models.CharField(max_length=10000)
    Person = models.ForeignKey(Person)
    time = models.DateTimeField()
    Question = models.ForeignKey(Question)
    def __str__(self):
        return self.answer


class Comment(models.Model):
    comment = models.CharField(max_length=10000)
    Person = models.ForeignKey(Person)
    Question = models.ForeignKey(Question)
    Answer = models.ForeignKey(Answer)
    def __str__(self):
        return self.comment


#     Person = models.ForeignKey(Person)
#     Question = models.ForeignKey(Question)
# # class Upvote(models.Model):
#     upvote=models.IntegerField(default=0)
#     Person=models.ForeignKey(Person)
#     Questions = models.ForeignKey(Questions)
#
#     class Meta(object):
#         unique_together = [('person', 'questions')]
#
# class Downvote(models.Model):
#     upvote = models.IntegerField(default=0)
#     Person=models.ForeignKey(Person)
#     Questions=models.ForeignKey(Questions)
#
#     class Meta(object):
#         unique_together = [('person', 'questions')]




















